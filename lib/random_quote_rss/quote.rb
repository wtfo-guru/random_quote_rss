# frozen_string_literal: true

require 'nokogiri'
require 'open-uri'

#
# @module RandomQuoteRss
#
module RandomQuoteRss
  #
  # @class Quote
  #
  class Quote
    def initialize(use_curl = false)
      @use_curl = use_curl
    end

    def fetch(outfile)
      quote = self.quote
      if outfile.nil?
        puts quote
      else
        tgtdir = File.dirname(outfile)
        raise "Directory not found: #{tgtdir}" unless File.directory? tgtdir

        File.open(outfile, 'w') { |f| f.write quote + "\n" }
      end
    rescue Error => e
      warn e.message
      raise
    end

    def quote
      data = if @use_curl
               %x(curl -s 'https://www.quotedb.com/quote/quote.php?action=random_quote_rss')
             else
               URI.open('https://www.quotedb.com/quote/quote.php?action=random_quote_rss')
             end
      doc = Nokogiri::HTML(data)

      author = doc.xpath('//channel/item/title').text
      quote = doc.xpath('//channel/item/description').text
      # link = doc.xpath("//channel/item/link").text
      "#{quote} - #{author}"
    end
  end
end
